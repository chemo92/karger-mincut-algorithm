package com.progcraft.mincut.logic;

import java.util.List;
import java.util.Random;
import java.util.Set;

import com.progcraft.mincut.model.Graph;

public class MinCutAlgorithm {

	private static Random rnd = new Random();
	
	public static int calculate(Graph graph) {
		while(graph.getVerticesCount() != 2) {
			int[] edge = selectRandomEdge(graph);
			boolean success = graph.mergeEdge(edge[0], edge[1]);
			if(!success) {
				System.err.println("!!!!!!!!!");
			}
		}
		int vertex = (int)graph.getVertices().toArray()[0];
		return graph.getVertexAdjacencyList(vertex).size();
	}
	
	private static int[] selectRandomEdge(Graph graph) {
		int[] result = new int[2];
		Set<Integer> vertices = graph.getVertices();
		result[0] = (int)vertices.toArray()[rnd.nextInt(vertices.size())];
		List<Integer> adjecencyList = graph.getVertexAdjacencyList(result[0]);
		do {
			result[1] = selectRandomValue(adjecencyList);
		} while(result[0] == result[1]);
		return result;
	}
	
	private static int selectRandomValue(List<Integer> list) {
		return list.get(rnd.nextInt(list.size()));
	}
}
