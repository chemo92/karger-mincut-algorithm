package com.progcraft.mincut.logic;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.progcraft.mincut.model.Graph;

public class ApplicationEntry {

	public static void main(String[] args) throws IOException {
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < 1000; i++) {
			Map<Integer, List<Integer>> graph = ReadGraphFromFile.read("kargerMinCut.txt");
			int res = MinCutAlgorithm.calculate(new Graph(graph));
			if(res < min) {
				min = res;
				System.out.println(min);
			}
		}
		System.out.println(min);
	}
}
