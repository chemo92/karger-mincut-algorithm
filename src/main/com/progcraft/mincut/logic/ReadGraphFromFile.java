package com.progcraft.mincut.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadGraphFromFile {

	public static Map<Integer, List<Integer>> read(String fileName) throws IOException {
		Map<Integer, List<Integer>> graph = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	String[] values = line.split("\t");
		    	List<Integer> edgesVertices = new ArrayList<>(values.length);
		    	Integer vertexNumber = Integer.parseInt(values[0]);
		    	for(int i = 1; i < values.length; i++) {
		    		edgesVertices.add(Integer.parseInt(values[i]));
		    	}
		    	graph.put(vertexNumber, edgesVertices);
		    }
		}
		return graph;
	}
}
