package com.progcraft.mincut.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph {

	private Map<Integer, List<Integer>> graph;
	
	public Graph(Map<Integer, List<Integer>> graph) {
		this.graph = graph;
	}
	
	public int getVerticesCount() {
		return graph.size();
	}
	
	public List<Integer> getVertexAdjacencyList(int vertex) {
		return graph.get(vertex);
	}
	
	public Set<Integer> getVertices() {
		return graph.keySet();
	}
	
	public boolean mergeEdge(int vertex1, int vertex2) {
		boolean operationResult = false;
		List<Integer> vertices = graph.get(vertex1);
		if(vertices != null) {
			if(vertices.stream().anyMatch(value -> value.equals(vertex2))) {
				List<Integer> vertices2 = graph.get(vertex2);
				vertices2.removeIf(value -> value.equals(vertex1));
				vertices2.stream().forEach(value -> {
					List<Integer> adjecencyList = graph.get(value);
					int initialSize = adjecencyList.size();
					adjecencyList.removeIf(value2 -> value2.equals(vertex2));
					int toAdd = initialSize - adjecencyList.size();
					for(int i = 0; i < toAdd; i++) {
						adjecencyList.add(vertex1);
					}
				});
				vertices.removeIf(value -> value.equals(vertex2));
				vertices.addAll(vertices2);
				graph.remove(vertex2);
				graph.remove(vertex1);
				graph.put(vertex1, vertices);
				operationResult = true;
			}
		}
		return operationResult;
	}
}
