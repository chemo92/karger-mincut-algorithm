package com.progcraft.mincut.model;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.progcraft.mincut.logic.ReadGraphFromFile;

public class GraphTest {

	private Graph graph;
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void testMergeEdge() {
		graph = createGraph("test1.txt");
		boolean result = graph.mergeEdge(1, 3);
		assertTrue(result);
		assertEquals(3, graph.getVerticesCount());
		assertArrayEquals(new Integer[]{2, 2, 4}, graph.getVertexAdjacencyList(1).toArray());
		assertArrayEquals(new Integer[]{1, 1}, graph.getVertexAdjacencyList(2).toArray());
		assertArrayEquals(new Integer[]{1}, graph.getVertexAdjacencyList(4).toArray());
	}

	@Test
	public void testMergeEdgeWithRevercedVertices() {
		graph = createGraph("test1.txt");
		boolean result = graph.mergeEdge(3, 1);
		assertTrue(result);
		assertEquals(3, graph.getVerticesCount());
		assertArrayEquals(new Integer[]{2, 4, 2}, graph.getVertexAdjacencyList(3).toArray());
		assertArrayEquals(new Integer[]{3, 3}, graph.getVertexAdjacencyList(2).toArray());
		assertArrayEquals(new Integer[]{3}, graph.getVertexAdjacencyList(4).toArray());
	}
	
	@Test
	public void testMergeEdge2() {
		graph = createGraph("test1.txt");
		boolean result = graph.mergeEdge(1, 2);
		assertTrue(result);
		assertEquals(3, graph.getVerticesCount());
		assertArrayEquals(new Integer[]{3, 3}, graph.getVertexAdjacencyList(1).toArray());
		assertArrayEquals(new Integer[]{1, 4, 1}, graph.getVertexAdjacencyList(3).toArray());
		assertArrayEquals(new Integer[]{3}, graph.getVertexAdjacencyList(4).toArray());
	}

	
	@Test
	public void testMergeNotExistingEdge() {
		graph = createGraph("test1.txt");
		boolean result = graph.mergeEdge(2, 4);
		assertFalse(result);
	}
	
	@Test
	public void testMergeEdgeOnGraphWithOneEdge() {
		graph = createGraph("test2.txt");
		boolean result = graph.mergeEdge(1, 2);
		assertTrue(result);
		assertEquals(1, graph.getVerticesCount());
		assertArrayEquals(new Integer[]{}, graph.getVertexAdjacencyList(1).toArray());
	}
	
	private Graph createGraph(String fileName) {
		Graph result = null;
		try {
			result = new Graph(ReadGraphFromFile.read(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
